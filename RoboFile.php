<?php
/**
 * This is project's console commands configuration for Robo task runner.
 *
 * @see http://robo.li/
 */
class RoboFile extends \Robo\Tasks
{
    // define public methods as commands
    public function renew($id = false)
    {
        if (!$id) {
            $id = $this->ask("请输入要更新的包ID");
        }
        if (intval($id) < 2) {
            die("错误的id");
        }

        $this->_deleteDir(['app']);
        
        // $url = 'https://fox2.applinzi.com/ftcode/tree.'.$id.'.tar.gz';
        $this->_exec("wget https://fox2.applinzi.com/ftcode/tree." . $id . ".tar.gz -O myapp.tar.gz && tar xzvf myapp.tar.gz");

        $this->taskReplaceInFile('app/myapp/package.json')
        ->from('"copy-paste-cli": "^1.0.1"')
        ->to('"clipboard-cli": "^2.0.0"')
        ->run();

        $this->taskReplaceInFile('app/myapp/RoboFile.php')
        ->from('node_modules/copy-paste-cli/cli.js  copy')
        ->to('node_modules/clipboard-cli/cli.js')
        ->run();

        $this->_deleteDir(['app/myapp/node_modules']);
        $this->_remove('app/myapp/yarn.lock');

        $this->_exec("tar cvzf tree.$id.202009.tar.gz app/");
        $this->say("done");
    }

    public function clean()
    {
        $this->_deleteDir(['app']);
        $this->_remove('myapp.tar.gz');
    }
}
