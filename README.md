# ft-init-shells

全栈课用环境初始化shell脚本

## 目录
- 初始化express环境完成
   `curl -s https://gitlab.com/easychen/ft-init-shells/raw/master/001.sh | bash`
- GitHub登入流程完成
   `curl -s https://gitlab.com/easychen/ft-init-shells/raw/master/002.sh | bash`
- 会话和Sessoion
   `curl -s https://gitlab.com/easychen/ft-init-shells/raw/master/003.sh | bash` 
- 中间件和错误处理
   `curl -s https://gitlab.com/easychen/ft-init-shells/raw/master/004.sh | bash` 
- Mongoose使用
   `curl -s https://gitlab.com/easychen/ft-init-shells/raw/master/005.sh | bash`
- Bootstrap美化
   `curl -s https://gitlab.com/easychen/ft-init-shells/raw/master/006.sh | bash`
- Map方案实现detail    
   `curl -s https://gitlab.com/easychen/ft-init-shells/raw/master/007.sh | bash` 
- Subdocument方案实现detail    
   `curl -s https://gitlab.com/easychen/ft-init-shells/raw/master/008.sh | bash`
- Populate方案实现detail
   `curl -s https://gitlab.com/easychen/ft-init-shells/raw/master/009.sh | bash`

---


- SPA框架方案
   `curl -s https://gitlab.com/easychen/ft-init-shells/raw/master/101.sh | bash`  

- EasyStart3
   `curl -s https://gitlab.com/easychen/ft-init-shells/raw/master/es3.sh | bash` 

- 方糖知识树·SPA布局
   `curl -s https://gitlab.com/easychen/ft-init-shells/raw/master/102.sh | bash` 

- 方糖知识树·SPA编辑器
   `curl -s https://gitlab.com/easychen/ft-init-shells/raw/master/103.sh | bash`

- 方糖知识树·SPA编辑器·样式调整：编辑器全宽
   `curl -s https://gitlab.com/easychen/ft-init-shells/raw/master/104.sh | bash`   

- 方糖知识树·API开发①
   `curl -s https://gitlab.com/easychen/ft-init-shells/raw/master/105.sh | bash`  

- 方糖知识树·知识树开发完成
   `curl -s https://gitlab.com/easychen/ft-init-shells/raw/master/106.sh | bash`     

- 方糖知识树·笔记开发完成
   `curl -s https://gitlab.com/easychen/ft-init-shells/raw/master/107.sh | bash` 

- 方糖知识树·用户认证完成
   `curl -s https://gitlab.com/easychen/ft-init-shells/raw/master/108.sh | bash` 

- 方糖知识树·小屏优化完成
   `curl -s https://gitlab.com/easychen/ft-init-shells/raw/master/109.sh | bash`                 


curl -s https://gitlab.com/easychen/ft-init-shells/raw/master/tmp.sh | bash